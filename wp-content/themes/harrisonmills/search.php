<?php get_header(); ?>
	<div id="body">		
		<header id="body-header">
			<?php get_banner_image(); ?>
		</header>
		<div class="page-width">
			<section id="content">
				<h1 class="page-title">Search Results for: "<?php echo get_search_query(); ?>"</h1>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="post">
						<h2><?php the_title(); ?></h2>
						<?php the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>">Read More</a>
					</div>
				<?php endwhile; ?>
				<?php else: ?>
					<div class="post">
						<h2>Nothing Found</h2>
						<p>We couldn't find anything by that search term. Please try something else.</p>
					</div>
				<?php endif; ?>
			</section>
		</div>
	</article>	
<?php get_footer(); ?>