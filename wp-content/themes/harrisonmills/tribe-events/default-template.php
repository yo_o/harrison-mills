<?php get_header(); ?>
<div id="body">		
	<header id="body-header">
		<?php get_banner_image(); ?>
	</header>
	<div class="page-width">
		<section id="sidebar">
			<?php get_sidebar(); ?>
		</section>
		<section id="content">
			<div id="tribe-events-pg-template">
				<?php tribe_events_before_html(); ?>
				<?php tribe_get_view(); ?>
				<?php tribe_events_after_html(); ?>
			</div>
		</section>
	</div>
</article>	
<?php get_footer(); ?>