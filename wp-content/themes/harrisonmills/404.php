<?php get_header(); ?>
	
		<div id="body">		
			<header id="body-header">
				<?php get_banner_image(); ?>
			</header>
			<div class="page-width">
				<section id="sidebar">
					<?php get_sidebar(); ?>
				</section>
				<section id="content">
					<h1>Not Found</h1>
					<p>Sorry, the content/page you were looking for could not be found.</p>
					<p>Please try a link above or contact us if you think there may be an Error.</p>
				</section>
			</div>
		</div>	
	
<?php get_footer(); ?>