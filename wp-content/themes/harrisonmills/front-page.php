<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
	
		<div id="body">		
			<header id="body-header">
				<?php if($gallery = get_field('gallery')) : ?>
					<div class="cycle-slideshow"
						data-cycle-center-horz=true
						data-cycle-swipe=true
						data-cycle-loader="wait">
						<div class="cycle-pager"></div>
						<?php foreach($gallery as $image) : ?>
							<?php echo wp_get_attachment_image( $image['id'], 'homepage-gallery' ); ?>
						<?php endforeach; ?>

					</div>
				<?php endif; ?>


				
				
				<div class="page-width">
					<div class="left">
						<?php the_content(); ?>
					</div>
					<div class="right events-roll">
						<?php dynamic_sidebar( 'home-events' ); ?>
					</div>
				</div>
			</header>
			
			<section class="newsletter-signup">
				<div class="page-width">
					<div class="sprite signup-logo"></div>

					<form class="newsletter-signup-form" action="<?php echo home_url( 'harrison-mills/newsletter' ); ?>" method="get">
						<input type="text"  class="input" id="firstname" name="firstname" placeholder="First Name*">
						<input type="email" class="input" id="email" name="email" placeholder="Email Address*">
						<input type="submit" class="submit">
					</form>
				</div>
			</section>

			<?php // dynamic_sidebar('things-to-do'); ?>
			<?php if($widgets = get_field('widget')) : ?>
				<section class="things-to-do border-bottom">
					<div class="inner">	
						<div class="page-width inherit-height-parent">
							<h2><a href="<?php echo get_permalink(44); ?>">There is lots to do in Harrison Mills.</a></h2>
							<?php foreach($widgets as $widget) : ?>
								<div class="four-column inherit-height">
									<?php echo wp_get_attachment_image( $widget['image'], 'thumbnail' ); ?>
									<div class="text">
										<h3><?php echo $widget['above_title']; ?></h3>
										<h1><?php echo $widget['title']; ?></h1>
										<ul>
											<?php foreach($widget['links'] as $link) : ?>
												<li><a href="<?php echo get_permalink( $link->ID ); ?>"><?php echo $link->post_title; ?></a></li>
											<?php endforeach; ?>
											
										</ul>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</section>
			<?php endif; ?>

			

			<section class="map border-bottom">
				<!--<img src="<?php echo bloginfo('stylesheet_directory'); ?>/images/map.png" alt="map" class="center">-->
				<div class="page-width">
					<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d83355.30441671355!2d-121.94482357415865!3d49.24127119449518!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x548415f93688e743%3A0xbb4a3fd9f707c6b8!2sHarrison+Mills%2C+BC+V0M!5e0!3m2!1sen!2sca!4v1394550008292" height="380" width="940" frameborder="0"></iframe>
					
					<div class="left">
						<h3><a href="<?php echo home_url( '/map/' ); ?>">Harrison Mills is just<br>a short drive away.</a></h3>
						<?php if(get_field('drive_time', 13)) : ?>
							<table class="drive-time">
								<tr><th class="column1">Location</th><th class="column2">Time</th></tr>
								<?php foreach(get_field('drive_time', 13) as $data) : ?>
									<tr><td class="column1"><?php echo $data['location']; ?></td><td class="column2"><?php echo $data['time']; ?></td></tr>
								<?php endforeach; ?>
							</table>
						<?php endif; ?>
					</div>
				</div>
			</section>
			
			<section class="image-feed texture">
				<div class="page-width">
					<h2>Live Image Feed</h2>
					<p><a href="#">Upload images to Instagram feed</a></p>
				</div>
				<div class="instagram-feed">
					<div class="page-width">
						<?php echo do_shortcode('[si_feed limit="16" size="small" link="true"]'); ?>
					</div>
				</div>
				<?php if($hashtags = get_field('hashtags')): ?>
				<div class="page-width">
					<small>Share your images: 
						<?php 
							$content = '';
							foreach($hashtags as $hashtag) {
								$content .= '#'.$hashtag['hashtag'].' ';
							}
							echo $content;
						?>
						
						
					</small>
				</div>
				<?php endif; ?>
			</section>
			
		</div>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>