<?php if(is_page('map')) : //---------------------------------------------------------------	MAP SIDEBAR?>
	<h3>Drive Time</h3>
	<br>
	<?php if(get_field('drive_time', 13)) : ?>
		<table class="drive-time">
			<tr><th class="column1">Location</th><th class="column2">Time</th></tr>
			<?php foreach(get_field('drive_time', 13) as $data) : ?>
				<tr><td class="column1"><?php echo $data['location']; ?></td><td class="column2"><?php echo $data['time']; ?></td></tr>
			<?php endforeach; ?>
		</table>
	<?php endif; ?>

	<?php dynamic_sidebar('map'); ?>

<?php elseif(is_archive()) : //---------------------------------------------------------------	ARCHIVE SIDEBAR ?>
	<?php 
		switch(get_post_type()) {
			case 'concession':
				$title = 'Eat & Drink';
				$link_to_single = true;
				break;
			case 'accomodation':
				$title = 'Stay';
				$link_to_single = true;
				break;
			case 'attraction':
				$title = 'See & Do';
				$link_to_single = true;
				break;
		}
	?>
	<h3><?php echo apply_filters( 'the_title', $title ); ?></h3>
	<ul>
		<?php 
			$class = '';
			if(!get_query_var('term')) $class = 'current-menu-item'; 
		?>
		<li class="<?php echo $class; ?>"><a href="<?php echo home_url(); ?>/<?php echo get_post_type(); ?>/" title="All">All</a></li>
		<?php
			$categories = get_terms( get_post_type().'-categories', array(
				'hide_empty'		=> false,
				'orderby'				=> 'menu_order',
				'order'					=> 'ASC'
			)); 

			foreach ($categories as $category) {
				$class = '';
				if(get_query_var('term') == $category->slug) $class = 'current-menu-item';
				echo '<li class="'.$class.'"><a href="'.get_term_link( $category ).'" title="'.$category->name.'">'.$category->name.'</a>';
				if(get_query_var( get_post_type() .'-categories' ) == $category->slug){
					$category_posts = get_posts(array(
						'posts_per_page' 							=> -1,
						'orderby' 										=> 'menu_order',
						'post_type'	 									=> get_post_type(),
						get_post_type().'-categories' => $category->slug,
					));
					echo '<ul class="subnav">';
					foreach($category_posts as $category_post) {
						$class = '';
						if($category_post == $post) $class = 'current-menu-item';
						if($link_to_single){
							echo '<li class="'.$class.'"><a href="'.get_permalink( $category_post ).'" title="'.$category_post->post_title.'">'.$category_post->post_title.'</a></li>';	
						}else{
							echo '<li class="'.$class.'">'.$category_post->post_title.'</li>';
						}
						
					}
					echo '</ul>';
				}
				echo '</li>';
			}
		?>
	</ul>

<?php elseif(is_single() && get_post_type() != 'tribe_events') : //---------------------------------------------------------------	SINGLE SIDEBAR?>
	<?php 
		switch(get_post_type()) {
			case 'concession':
				$title = 'Eat & Drink';
				$post_categories = get_the_terms( $post->ID, 'concession-categories' );
				break;
			case 'accomodation':
				$title = 'Stay';
				$post_categories = get_the_terms( $post->ID, 'accomodation-categories' );
				break;
			case 'attraction':
				$title = 'See & Do';
				$post_categories = get_the_terms( $post->ID, 'attraction-categories' );
				break;
		}
	?>
	<h3><?php echo apply_filters( 'the_title', $title ); ?></h3>
	<ul>
		<?php 
			if($post_categories) $post_category = array_pop($post_categories);
			$class = '';
			if($post_categories) $class = 'current-menu-item'; 
		?>
		<li class="<?php echo $class; ?>"><a href="<?php echo home_url(); ?>/<?php echo get_post_type(); ?>/" title="All">All</a></li>
		<?php
			$categories = get_terms( get_post_type().'-categories', array(
				'hide_empty'		=> false,
				'orderby'				=> 'menu_order',
				'order'					=> 'ASC'
			)); 
			foreach ($categories as $category) {
				$class = '';
				if($post_category->slug == $category->slug) {
					echo '<li class="current-menu-item"><a href="'.get_term_link( $category ).'" title="'.$category->name.'">'.$category->name.'</a>';
						$category_posts = get_posts(array(
							'posts_per_page' 							=> -1,
							'orderby' 										=> 'menu_order',
							'post_type'	 									=> get_post_type(),
							get_post_type().'-categories' => $category->slug,
						));
						echo '<ul class="subnav">';
						foreach($category_posts as $category_post) {
							$class = '';
							if($category_post == $post) $class = 'current-menu-item';
							echo '<li class="'.$class.'"><a href="'.get_permalink( $category_post ).'" title="'.$category_post->post_title.'">'.$category_post->post_title.'</a></li>';
						}
						echo '</ul>';
					echo '</li>';

				}else{
					echo '<li><a href="'.get_term_link( $category ).'" title="'.$category->name.'">'.$category->name.'</a></li>';

				} 
				
			}
		?>
	</ul>
<?php elseif(get_post_type() == 'tribe_events') : ?>
	<?php
		if(get_query_var('eventDisplay')) :
			if(get_query_var('eventDisplay') == 'month') {
				$list 		= '';
				$calendar = 'current-menu-item';
			}
			if(get_query_var('eventDisplay') == 'upcoming') {
				$list 		= 'current-menu-item';
				$calendar = '';
			}
	?>
	<h3>Events</h3>
	<ul>
		<li class="<?php echo $list; ?>"><a href="<?php echo home_url('/events/'); ?>">Events List All</a></li>
		<li class="<?php echo $calendar; ?>"><a href="<?php echo home_url('/events/month/'); ?>">Event Calendar</a></li>
	</ul>
	<?php endif; ?>
<?php else : //--------------------------------------------------------------- PAGE SIDEBAR?>
	<?php $topmost_page = get_post(get_topmost_parent($post->ID)); ?>
	<h3><?php echo apply_filters( 'the_title', $topmost_page->post_title ); ?></h3>
	<ul>
		<?php 
			if($topmost_page->ID == 15) {
				wp_list_pages(array(
					'include' 	=> $topmost_page->ID,
					'title_li'	=> '',
				));
			}
		?>
		<?php
			wp_list_pages(array(
				'child_of' 	=> $topmost_page->ID,
				'title_li'	=> '',
			));
		?>
	</ul>

<?php endif; ?>