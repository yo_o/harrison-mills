<?php get_header(); ?>
	<div id="body">		
		<header id="body-header">
			<?php get_banner_image(); ?>
		</header>
		<div class="page-width">
			<?php if ( have_posts() ) : ?>
				<section id="page-header">
					<?php google_map(); ?>
				</section>
				<section id="sidebar">
					<?php get_sidebar(); ?>
				</section>
				<section id="content">
					<?php $wp_query->set( 'orderby', 'title' ); ?>
					<?php  while ( have_posts() ) : the_post(); ?>
						<div class="post <?php echo get_post_type(); ?>">
							<?php
								$content = '';
								$content .= '<div class="left">';
								if ( has_post_thumbnail() ) $content .= get_the_post_thumbnail( $post->ID, 'thumbnail');
								$content .= '<h2>Amenities</h2>';
								$content .= '<div class="amenities-grid">';
								$count = 1;
								
								foreach(get_the_terms( $post->ID, 'amenities' ) as $amenity) {
									if(get_field('icon', 'amenities_'.$amenity->term_id)){
										$class = '';
										if($count % 3 == 0) {
											$class = 'last';
										}

										$content .= '<img src="'.get_field('icon', 'amenities_'.$amenity->term_id).'" alt="'.$amenity->name.'" class="'.$class.'">';
										$count ++;
									}								
								}
								$content .= '</div>';
								$content .= '</div>';
								
								$content .= '<div class="right">';
								if(get_field('logo')) {
									$image = get_field('logo');
									$content .= '<div class="left">';
									$content .= '<img src="'.$image['sizes']['thumbnail'].'">';
									$content .= '</div>';
								}
								$content .=  '<h1 class="clear"><a href="'.get_permalink().'">'.get_the_title().'</a></h1>';
								$content .=  '<div class="clear">'.get_the_excerpt().'</div>';
								$content .= '</div>';
								
								echo apply_filters('the_content', $content);
							?>
						</div>
						
					<?php endwhile; ?>
				</section>
			<?php else : ?>
				<section id="content">
					<h1>Nothing found</h1>
				</section>
			<?php endif; ?>
		</div>
	</div>	
<?php get_footer(); ?>