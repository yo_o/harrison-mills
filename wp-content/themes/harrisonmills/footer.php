	<footer>
		<div class='page-width'>
			<div class="column-seven social">
				<h3>Find us on</h3>
				<ul>
					<li><a href="https://twitter.com/HarrisonMillsBC" target="_blank" class="twitter"><div></div><span>Twitter</span></a></li>
					<li><a href="https://facebook.com/TourismHarrisonMills" target="_blank" class="facebook"><div></div><span>Facebook</span></a></li>
					<li><a href="http://instagram.com/Tourismharrisonmills" target="_blank" class="instagram"><div></div><span>Instagram</span></a></li>
					<li><a href="http://www.tripadvisor.ca/Tourism-g499135-Harrison_Mills_British_Columbia-Vacations.html" target="_blank" class="tripadvisor"><div></div><span>tripadvisor</span></a></li>
					<li><a href="https://plus.google.com/108211267406886786411" target="_blank" class="google"><div></div><span>Google +</span></a></li>
					<li><a href="http://www.youtube.com/user/TourismHarrisonMills" target="_blank" class="youtube"><div></div><span>YouTube</span></a></li>
				</ul>
			</div>
			<div class="column-seven">
				<h3>See & Do</h3>
				<ul>
					<li><a href="<?php echo home_url(); ?>/attraction/" title="All See & Do">All</a></li>
					<?php
						$categories = get_terms('attraction-categories', array(
							'hide_empty'		=> false,
							'orderby'				=> 'menu_order',
							'order'					=> 'ASC'
						)); 
						foreach ($categories as $category) {
							echo '<li><a href="'.get_term_link( $category ).'" title="'.$category->name.'">'.$category->name.'</a></li>';
						}
					?>
				</ul>
			</div>
			<div class="column-seven">
				<h3>Eat & Drink</h3>
				<ul>
					<li><a href="<?php echo home_url(); ?>/concession/" title="All Eat & Drink">All</a></li>
					<?php
						$categories = get_terms('concession-categories', array(
							'hide_empty'		=> false,
							'orderby'				=> 'menu_order',
							'order'					=> 'ASC'
						)); 
						foreach ($categories as $category) {
							echo '<li><a href="'.get_term_link( $category ).'" title="'.$category->name.'">'.$category->name.'</a></li>';
						}
					?>
				</ul>
			</div>
			<div class="column-seven">
				<h3>Stay</h3>
				<ul>
					<li><a href="<?php echo home_url(); ?>/accomodation/" title="All Accomodations">All</a></li>
					<?php
						$categories = get_terms('accomodation-categories', array(
							'hide_empty'		=> false,
							'orderby'				=> 'menu_order',
							'order'					=> 'ASC'
						)); 
						foreach ($categories as $category) {
							echo '<li><a href="'.get_term_link( $category ).'" title="'.$category->name.'">'.$category->name.'</a></li>';
						}
					?>
				</ul>
			</div>
			<div class="column-seven">
				<h3>Events</h3>
				<ul>
					<li><a href="<?php echo home_url( '/events/' ); ?>">Events List</a></li>
					<li><a href="<?php echo home_url( '/events/month/' ); ?>">Event Calendar</a></li>
				</ul>
			</div>
			<div class="column-seven">
				<h3><a href="<?php echo get_permalink(13); ?>">Map</a></h3>
			</div>
			<div class="column-seven">
				<h3>Harrison Mills</h3>
				<ul>
					<?php 
						wp_list_pages(array(
							'child_of' 		=> 15,
							'title_li'		=> '',
							'sort_column'	=> 'menu_order'
						)); 
					?>
				</ul>
			</div>
			
			<div class="cc-credit">
				<a href="http://www.tourismharrison.com" target="_blank" class="sprite tourism-hhs">Tourism Harrison Hot Springs</a>
				<a href="http://604pulse.com" target="_blank" class="sprite tourism-tbc">Travel BC</a>
			</div>
			
			<div class="tripadvisor-credit">
				<div class="top">
					<span>Ratings provided by</span> <a href="http://www.tripadvisor.ca/Tourism-g499135-Harrison_Mills_British_Columbia-Vacations.html" target="_blank" class="sprite"></a>
				</div>
				<div class="bottom">
					Check out what other travellers say about <a href="http://www.tripadvisor.ca/Tourism-g499135-Harrison_Mills_British_Columbia-Vacations.html" target="_blank" >Harrison Mills</a> on TripAdvisor.
				</div>
			</div>
			
			<div class="final-credit">
				<div class="left">
					All Rights Reserved, Tourism Harrison Mills © 2014
				</div>
				<div class="right">
					<a href="<?php echo get_permalink(17); ?>">Media</a> | <a href="<?php echo get_permalink(19); ?>">Partners</a> | <a href="<?php echo get_permalink(21); ?>">Weather Cam</a> | <a href="<?php echo get_permalink(23); ?>">Contact</a> | <a href="<?php echo get_permalink(54); ?>">Privacy Policy</a>
				</div>
			</div>
		</div>
	</footer>
<?php wp_footer(); ?>

</body>
</html>