<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div id="body">		
			<header id="body-header">
				<?php get_banner_image(); ?>
			</header>
			<div class="page-width">
				<?php  if ( get_field('page_header') ) : ?> 
					<section id="page-header">
						<?php the_field('page_header'); ?>

						
					</section>
				<?php endif; ?>
				<section id="sidebar">
					<?php get_sidebar(); ?>
				</section>
				<section id="content">
					<?php the_content(); ?>
				</section>
			</div>
			
			<?php if(is_page('itineraries')) : ?>
				<?php if($widgets = get_field('widget', 2)) : ?>
					<section class="things-to-do border-bottom">
						<div class="inner">	
							<div class="page-width inherit-height-parent">
								<h2><a href="<?php echo get_permalink(44); ?>">There is lots to do in Harrison Mills.</a></h2>
								<?php foreach($widgets as $widget) : ?>
									<div class="four-column inherit-height">
										<?php echo wp_get_attachment_image( $widget['image'], 'thumbnail' ); ?>
										<div class="text">
											<h3><?php echo $widget['above_title']; ?></h3>
											<h1><?php echo $widget['title']; ?></h1>
											<ul>
												<?php foreach($widget['links'] as $link) : ?>
													<li><a href="<?php echo get_permalink( $link->ID ); ?>"><?php echo $link->post_title; ?></a></li>
												<?php endforeach; ?>
												
											</ul>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</section>
				<?php endif; ?>
			<!--
				<section class="things-to-do border-top">
			 		<div class="inner">	
			 			<div class="page-width">
			 				<h2>There is lots to do in Harrison Mills.</h2>
							<?php dynamic_sidebar('things-to-do'); ?>
			 			</div>
			 		</div>
				</section>
			-->
			<?php endif; ?>
			
		</div>	
	<?php endwhile; endif; ?>
<?php get_footer(); ?>