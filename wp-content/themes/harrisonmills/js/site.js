jQuery(document).ready(function($) {
	
	if($('body').hasClass('single-accomodation')){
		$(window).load(function() {
			$('a.cbox-group').colorbox({rel:'cbox-group'});
		});
	}

	if($('body').hasClass('page-newsletter')){
		var $_GET = {};
		document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
			function decode(s) {
				return decodeURIComponent(s.split("+").join(" "));
			}
			$_GET[decode(arguments[1])] = decode(arguments[2]);
		});

		if($_GET["firstname"].length > 0) {
			$('#mce-FNAME').val($_GET["firstname"]);
		}

		if($_GET["email"].length > 0) {
			$('#mce-EMAIL').val($_GET["email"]);
		}
	}

	if($('.newsletter-signup-form').length > 0) {
		$('.newsletter-signup-form').submit(function(){
			if ( $(".newsletter-signup-form #firstname" ).val().length > 0 && $(".newsletter-signup-form #email" ).val().length > 0 ) {
			  return;
			}
			event.preventDefault();
		});
	}

	if($(window).width() < 721) {
		$('#mobile-nav-trigger').click(function(){
			$('#menu-primary').slideToggle();
			event.preventDefault();
		})
		
	}else{
		if($('.inherit-height-parent').length > 0){
			$(window).load(function() {
				$('.inherit-height-parent').each(function(){
					var parent = $(this);
					var children = parent.children('.inherit-height');
					var maxHeight = -1;
					children.each(function() {
						maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
					});
					children.each(function() {
						$(this).height(maxHeight);
					});
				});
			});	
		}
	}

});

