<?php
function get_banner_image(){
	$src = get_bloginfo('stylesheet_directory') . '/images/banners/HarrisonMills_BannerImages_HarrisonMills.jpg';	
	
	if(get_post_type() == 'attraction') {
		$src = get_bloginfo('stylesheet_directory') . '/images/banners/HarrisonMills_BannerImages_SeeDo.jpg';	
	}
	if(get_post_type() == 'concession') {
		$src = get_bloginfo('stylesheet_directory') . '/images/banners/HarrisonMills_BannerImages_Eat.jpg';	
	}
	if(get_post_type() == 'accomodation') {
		$src = get_bloginfo('stylesheet_directory') . '/images/banners/HarrisonMills_BannerImages_Stay.jpg';	
	}
	if(is_page('map')) {
		$src = get_bloginfo('stylesheet_directory') . '/images/banners/HarrisonMills_BannerImages_Map.jpg';	
	}
	if(get_post_type() == 'tribe_events') {
		$src = get_bloginfo('stylesheet_directory') . '/images/banners/HarrisonMills_BannerImages_Events.jpg';	
	}
	echo '<div class="banner-image" style="background-image:url('.$src.');"></div>';
}
//--------------------------------------------------------------- 
function wpprogrammer_post_name_in_body_class( $classes ){
  if(is_singular()) {
    global $post;
    array_push( $classes, "{$post->post_type}-{$post->post_name}" );
  }
  return $classes;
}
add_filter( 'body_class', 'wpprogrammer_post_name_in_body_class' );
//---------------------------------------------------------------
add_filter('show_admin_bar', '__return_false'); 
//---------------------------------------------------------------
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' ); 
add_theme_support( 'widgets' );
//---------------------------------------------------------------	
if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'homepage-gallery', 1200, 425 );
}
//---------------------------------------------------------------	
function theme_name_scripts() {
	wp_deregister_style('simple-instagram-plugin-styles');
	wp_deregister_style('ssba-page-styles');
	remove_action('wp_head', 'get_ssba_style');
	
	wp_deregister_script('jquery');
	wp_register_script(	'jquery', ("//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"), false, '1.9.1', true);
	//wp_register_script( 'nivo', get_template_directory_uri() . '/js/jquery.nivo.slider.pack.js', false, '3.2', true );
	wp_register_script( 'cycle2-center', get_template_directory_uri() . '/js/jquery.cycle2.center.min.js', false, '2.1.3', true );
	wp_register_script( 'cycle2-swipe', get_template_directory_uri() . '/js/jquery.cycle2.swipe.min.js', false, '2.1.3', true );
	wp_register_script( 'cycle2', get_template_directory_uri() . '/js/jquery.cycle2.min.js', false, '2.1.3', true );
	wp_register_script( 'colorbox', get_template_directory_uri() . '/js/jquery.colorbox-min.js', false, '1.5.4', true );

	if(is_front_page())	{
		//$deps = array('jquery', 'nivo');
		$deps = array('jquery', 'cycle2', 'cycle2-center', 'cycle2-swipe', );
	}elseif(is_single() && get_post_type() == 'accomodation'){
		$deps = array('jquery', 'colorbox');
	}else{
		$deps = array('jquery');
	}
	
	wp_enqueue_script( 'site', get_template_directory_uri() . '/js/site.js', $deps, '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
//---------------------------------------------------------------	
function get_topmost_parent($post_id){
  $parent_id = get_post($post_id)->post_parent;
  if($parent_id == 0){
    return $post_id;
  }else{
    return get_topmost_parent($parent_id);
  }
}
//---------------------------------------------------------------	
function widgets_init() {
  register_sidebar(array(
  	'name' => 'Map Page Sidebar',
  	'id' => 'map',
  	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
  	'after_widget' => '</aside>',
  	'before_title' => '<h3>',
  	'after_title' => '</h3>',
  ));
  register_sidebar(array(
  	'name' => 'Home Page Events',
  	'id' => 'home-events',
  	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
  	'after_widget' => '</aside>',
  	'before_title' => '<h3>',
  	'after_title' => '</h3>',
  ));

}
add_action( 'init', 'widgets_init' );
//---------------------------------------------------------------	
function google_map(){
	if ( function_exists( 'pronamic_google_maps_mashup' ) ) {
		$options = array(
			'width'          	=> 940,
			'height'         	=> 380, 
			'nopaging'				=> true,
			'marker_options' 	=> array(
				'icon' => get_template_directory_uri().'/images/marker.png'
			)
		);
		
		if(is_single()) {
			global $post;
			$query = array(
				'post_type' => get_post_type(),
				'p'					=> $post->ID
			);
		}elseif(get_query_var('term')) {
			$query = array(
				'post_type' => get_post_type(),
				get_post_type().'-categories' => get_query_var('term'),
			);
		}else{
			$query = array(
				'post_type' => get_post_type(),
			);
		}
		
		pronamic_google_maps_mashup( $query, $options);
	}
}
//---------------------------------------------------------------
function _handle_form_action(){
	var_dump($_POST['firstname']);
}
add_action('admin_post_submit-form', '_handle_form_action'); // If the user is logged in
add_action('admin_post_nopriv_submit-form', '_handle_form_action'); // If the user in not logged in

////---------------------------------------------------------------
//function add_background_images() {
//  global $post;
//  if(has_post_thumbnail()) {
//    $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), full );
//    echo '
//      <style type="text/css" media="screen">
//        html { 
//          background: url('.$thumbnail[0].') no-repeat center center fixed; 
//          -webkit-background-size: cover;
//          -moz-background-size: cover;
//          -o-background-size: cover;
//          background-size: cover;
//        }
//      </style>
//    ';
//  } 
//}
//add_action( 'wp_print_styles', 'add_background_images' );
////---------------------------------------------------------------
//function my_deregister_styles() {
//  wp_deregister_style('contact-form-7');
//  wp_deregister_style('shadowbox-css');
//  wp_deregister_style('shadowbox-extras');
//}
//add_action( 'wp_print_styles', 'my_deregister_styles', 100 );
////---------------------------------------------------------------
//function deregister_cf7_js() {
//  if ( !is_page('contact-us')) {
//    wp_deregister_script( 'contact-form-7');
//  }
//}
//add_action( 'wp_print_scripts', 'deregister_cf7_js' );
////---------------------------------------------------------------
//function quickchic_widgets_init() {
//  register_sidebar(array(
//  'name' => 'Homepage',
//  'id' => 'homepage',
//  'before_widget' => '',
//  'after_widget' => '',
//  'before_title' => '<h1>',
//  'after_title' => '</h1>',
//  'before_widget' => '',
//  'after_widget'  => '',
//  ));
//}
//add_action( 'init', 'quickchic_widgets_init' );
////---------------------------------------------------------------
//class RecentWidgetsCustom extends WP_Widget {
//
//  function RecentWidgetsCustom() {
//    parent::__construct( false, 'Recent Widgets || Custom' );
//  }
//
//  function widget( $args, $instance ) {
//    echo '<li id="recent-posts-custom" class="home-widget widget_recent_entries"><h1>Latest News</h1><ul>';
//    $args = array(
//      'post_type' => 'post',
//      'posts_per_page' => 2,
//      'orderby' => 'date',
//      'order' => 'DESC',
//    );
//    $post_query = new WP_Query( $args );
//    while ( $post_query->have_posts() ) : $post_query->the_post();
//      echo '<li>';
//      echo '<div class="post-date">'.get_the_time('F j, Y').'</div>';
//      echo '<p>'.get_the_title().'. <a href="'.get_permalink().'">More</a></p>';
//      echo '</li>';
//    endwhile;
//    wp_reset_postdata();
//    echo '</ul></li>';
//  }
//
//  function update( $new_instance, $old_instance ) {
//    
//  }
//
//  function form( $instance ) {
//    echo '<p>That\'s it, your done. Thank you.</p>';
//  }
//}
//
//function exitasset_register_widgets() {
//  register_widget( 'RecentWidgetsCustom' );
//}
//add_action( 'widgets_init', 'exitasset_register_widgets' );
////---------------------------------------------------------------
//function iggy_auto_excerpt_more( $more ) {
//  return '… ' . iggy_continue_reading_link();
//}
//add_filter( 'excerpt_more', 'iggy_auto_excerpt_more' );
//////--------------------------------------------------------------- THE READMORE FUNCTION
//function iggy_continue_reading_link() {
//  return ' <a href="'. esc_url( get_permalink() ) . '">' . __( 'read more', 'twentyeleven' ) . '</a>';
//}
////---------------------------------------------------------------

////--------------------------------------------------------------- CONTACT FORM SCRIPT
//function my_deregister_javascript() {
//  if ( !is_page('contact') ) {
//    wp_deregister_script( 'contact-form-7' );
//  }else{
//    if ( !is_admin() ) wp_deregister_script('jquery');
//  }
//}
//add_action( 'wp_print_scripts', 'my_deregister_javascript', 100 );