<?php get_header(); ?>
	<div id="body">		
		<header id="body-header">
			<?php get_banner_image(); ?>
		</header>
		<div class="page-width">
			<?php  if ( get_field('page_header') ) : ?> 
				<section id="page-header">
					<?php the_field('page_header'); ?>
				</section>
			<?php endif; ?>
			<section id="sidebar">
				<?php the_content(); ?>
			</section>
			<section id="content">
				<div class="breadcrumbs">
				    <?php if(function_exists('bcn_display')) bcn_display(); ?>
				</div>
				<?php the_content(); ?>
			</section>
		</div>
	</article>	
<?php get_footer(); ?>