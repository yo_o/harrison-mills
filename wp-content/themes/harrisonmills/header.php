<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title(''); ?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<!--[if lt IE 9]>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
<![endif]-->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<header class="main">	
		<div class="top">
			<div class="page-width">
				<div class="left">
					<a href="<?php echo home_url(); ?>" id="logo" class="sprite"><?php wp_title(''); ?></a>
					<div>
						It's in<br>
						our nature
					</div>
				</div>
				<div class="right">
					<?php wp_nav_menu(array( 'menu'=>'secondary', 'container'=>'nav', )); ?>
					<div class="searchform mobile-hidden">
						<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
						  <div>
						    <input type="text" value="" name="s" id="s" placeholder="Search"/>
						    <span class="border"></span>
						    <input type="submit" id="searchsubmit" value="Search" />
						  </div>
						</form>
					</div>
					
				</div>
				
			</div>
		</div>
		<div class="bottom">
			<div class="page-width">
				<a href="#" id="mobile-nav-trigger"><div></div></a>

				<div class="searchform mobile-visible">
					<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
					  <div>
					    <input type="text" value="" name="s" id="s" placeholder="Search"/>
					    <span class="border"></span>
					    <input type="submit" id="searchsubmit" value="Search" />
					  </div>
					</form>
				</div>

				<?php wp_nav_menu(array( 'menu'=>'primary', 'container'=>'nav', )); ?>
			</div>
		</div>
  </header>
