<?php get_header(); ?>
	<div id="body">		
		<header id="body-header">
			<?php get_banner_image(); ?>
		</header>
		<div class="page-width">
			<?php if ( have_posts() ) : ?>
				<section id="page-header">
					<?php google_map(); ?>
				</section>
				<section id="sidebar">
					<?php get_sidebar(); ?>
				</section>
				<section id="content">
					<?php $wp_query->set( 'orderby', 'title' ); ?>
					<?php  while ( have_posts() ) : the_post(); ?>
						<div class="post <?php echo get_post_type(); ?>">
							<?php 
								$content = ''; 
								if ( has_post_thumbnail() ) $content .= get_the_post_thumbnail( $post->ID, 'thumbnail');
								$content .= '<h1>'.get_the_title().'</h1>';
								$content .= get_the_content();
								
								if(get_field('address') || get_field('website')) {
									$content .= '<h2>Contact Information</h2>';
									$content .= '<div class="left">';
									if(get_field('address')) $content .= get_field('address');
									if(get_field('website')) $content .= '<br><br><a href="http://'.get_field('website').'" target="_blank">'.get_field('website').'</a>';
									$content .= '</div>';
								}
								
								if(get_field('logo')) {
									$image = get_field('logo');
									$content .= '<div class="right">';
									$content .= '<img src="'.$image['sizes']['thumbnail'].'">';
									$content .= '</div>';
								}
								
								echo apply_filters('the_content', $content);
							?>
							
						</div>
						
					<?php endwhile; ?>
				</section>
			<?php else : ?>
				<section id="content">
					<h1>Nothing found</h1>
				</section>
			<?php endif; ?>
		</div>
	</div>	
<?php get_footer(); ?>