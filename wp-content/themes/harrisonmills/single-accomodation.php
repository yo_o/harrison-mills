<?php get_header(); ?>
	<div id="body">		
		<header id="body-header">
			<?php get_banner_image(); ?>
		</header>
		<div class="page-width">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<section id="sidebar">
					<?php get_sidebar(); ?>
				</section>
				<section id="content">
					<div class="breadcrumbs">
						<?php if(function_exists('bcn_display')) bcn_display(); ?>
					</div>
					<?php 
						if(get_field('gallery')) {
							$gallery = get_field('gallery');
							echo '<div id="colorbox">';
							foreach($gallery as $count => $image){
								if($count == 0){
									echo '<a class="cbox-group" href="'.$image['url'].'"><img class="cbox-group" src="'.$image['sizes']['medium'].'" alt=""></a>';
								}else{
									echo '<a class="cbox-group" href="'.$image['url'].'" style="display:none;">&nbsp;</a>';
								}
							}
							echo '</div>';
						}
					?>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<hr>
					<h2>Amenities</h2>
					<div class="amenities-grid">
						<?php
							foreach(get_the_terms( $post->ID, 'amenities' ) as $amenity) {
								if(get_field('icon', 'amenities_'.$amenity->term_id)){
									$class = '';
									if($count % 3 == 0) $class = 'last';
									echo '<div class="two-column"><img src="'.get_field('icon', 'amenities_'.$amenity->term_id).'" alt="'.$amenity->name.'" class="'.$class.'"> <span>'.$amenity->name.'</span></div>';
									$count ++;
								}								
							}
						?>
					</div>	
					<hr>
					<?php if(get_field('address') || get_field('website')) : ?>						
						<h2>Contact Information</h2>
						<div class="left">
							<?php if(get_field('address')) the_field('address'); ?>
							<?php if(get_field('website')) echo '<br><br><a href="http://'.get_field('website').'" target="_blank">'.get_field('website').'</a>'; ?>
						</div>
					<?php endif; ?>
					
					<?php if(get_field('logo')): ?>
						<?php $image = get_field('logo'); ?>
						<div class="right">
							<img src="<?php echo $image['sizes']['thumbnail']; ?>">
						</div>
					<?php endif; ?>
					
					<?php echo do_shortcode('[ssba] '); ?>
				</section>
			<?php endwhile; endif; ?>
		</div>
	</div>	
<?php get_footer(); ?>