=== WeatherButton Widget from the Weather Network ===
Contributors: sajmeri, Jhurtubise  
Donate link: 
Tags: weather, widget, weather button,weatherbutton, the weather network, theweathernetwork, forecast, weather forecast, current conditions, 
Requires at least: 3.0
Tested up to: 3.32
Stable tag: trunk

WeatherButton Widget for Wordpress from The Weather Network

== Description ==

WeatherButton Widgets for Wordpress from the Weather Network

Now it's easier than ever to keep your site visitors informed of up-to-date weather conditions. Simply upload a WeatherButton Widget from The Weather Network on  your Wordpress powered website and follow the instructions to offer new content to your site visitors.

With two options available, light and dark versions, The Weather Network WeatherButton Widget blends perfectly in your site's design.

http://www.theweathernetwork.com/weatherplugin/wordpress/twn-weather.zip


== Installation ==

1. Download the file twn-weather.zip from 
http://www.theweathernetwork.com/weatherplugin/wordpress/twn-weather.zip
2. Upload `twn-weather.zip` to the `/wp-content/plugins/` directory of your site
3. Activate the plugin through the 'Plugins' menu in WordPress
4. Choose the city location code at http://www.theweathernetwork.com/weather_centre/wcwxbutton


== Frequently Asked Questions ==

Send your questions and comments at: 
http://media.theweathernetwork.com/contact.php


== Screenshots ==

1. The light version of the Weatherbutton.(screenshot-1.jpg) 
2. The dark version of the Weatherbutton. (screenshot-2.jpg)

== Changelog ==

= 1.0 
* First release.

== Upgrade Notice ==

= 1.0 = First release. Please come back often to http://www.theweathernetwork.com/weather_centre/wcwxbutton to be informed of new releases. 
